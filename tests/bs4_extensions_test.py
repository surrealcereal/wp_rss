from typing import Optional

from bs4 import Tag, NavigableString
from pytest import raises
from tests.test_utils import get_first_tag
from wprss.bs4_extensions import optional_capable_find, find_tag, find_tag_or_raise, find_or_raise, get_str_or_raise

t: Tag = get_first_tag("<b>Testing</b>")


def test_optional_capable_find() -> None:
    # not found
    assert optional_capable_find(t, "h2", Tag, optional=True) is None
    with raises(ValueError) as returned_none:
        optional_capable_find(t, "h2", Tag, optional=False)
    assert "returned None" in str(returned_none)
    assert optional_capable_find(t, "b", int, optional=True) is None
    with raises(ValueError) as wrong_return_type:
        optional_capable_find(t, "b", int, optional=False)
    assert "not of type" in str(wrong_return_type)
    result: Optional[Tag] = optional_capable_find(t, "b", Tag, optional=False)
    assert result is not None
    assert result.text == t.text
    class_test: Tag = get_first_tag("<div class='testing'>Hello</div>")
    class_test_result: Optional[Tag] = optional_capable_find(class_test, "div", Tag, optional=False,
                                                             class_="testing")
    assert class_test_result is not None
    assert class_test.text == class_test_result.text


class TestPartialledFindWrappers:
    @staticmethod
    def test_find_or_raise() -> None:
        with raises(ValueError) as returned_none:
            find_or_raise(t, "h2", Tag)
        assert "returned None" in str(returned_none)
        result: Tag | NavigableString = find_or_raise(t, "b", Tag)
        assert isinstance(result, Tag)
        assert result.text == t.text

    @staticmethod
    def test_find_tag_or_raise() -> None:
        with raises(ValueError) as returned_none:
            find_tag_or_raise(t, "h2")
        assert "returned None" in str(returned_none)
        result: Tag = find_tag_or_raise(t, "b")
        assert result.text == t.text

    @staticmethod
    def test_find_tag() -> None:
        assert find_tag(t, "h2") is None
        result: Optional[Tag] = find_tag(t, "b")
        assert result is not None
        assert result.text == t.text


def test_get_str_or_raise() -> None:
    membered_tag: Tag = get_first_tag("<img href='string'></img>", wrap=False)
    with raises(ValueError) as not_found:
        get_str_or_raise(membered_tag, "nonexistant")
    assert "not found" in str(not_found)
    assert get_str_or_raise(membered_tag, "href") == "string"
