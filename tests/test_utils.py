from pathlib import Path
from typing import Callable, Iterator, Protocol
from pickle import load as pickle_load

from bs4 import PageElement, Tag, BeautifulSoup


def declare_resources(root: Path, *resources: str) -> None:
    for resource in resources:
        if not (p := root / resource).exists():
            raise FileNotFoundError(f"Expected {p} to exist to run test")  # pragma: no cover


def get_resource_root(tested_file: str, caller_name: str = "") -> Path:
    return Path(f"./tests/resources/{tested_file}/{caller_name}/")


def compare_with_pickled[TestInput, TestResult](
        res_root: Path, consume_path: str,
        *,
        tested_func: Callable[[TestInput], TestResult],
        # IGNORE: str can be the same as TestInput, hence the identity function can work (if so). mypy should really
        #         actively check for that at the call-site instead of plainly complaining they don't match up 1-to-1.
        input_transformer: Callable[[str], TestInput] = lambda x: x,  # type: ignore[assignment, return-value]
        pickle_path: str = "expected.pickle"
) -> None:
    # res_root.mkdir(parents=True, exist_ok=True)
    declare_resources(res_root, consume_path, pickle_path)
    with open(res_root / consume_path, "r") as f:
        input_to_tested_func: str = "".join(f.readlines())
    to_compare: TestResult = tested_func(input_transformer(input_to_tested_func))
    with open(res_root / pickle_path, "rb") as p:
        expected: TestResult = pickle_load(p)
    if isinstance(to_compare, Iterator):
        # Collect elements to compare, otherwise checks if pointers are equal
        assert isinstance(expected, Iterator)
        assert tuple(to_compare) == tuple(expected)
    else:
        assert to_compare == expected


def get_first_tag(*elements: str, wrap: bool = True) -> Tag:
    html: str
    if len(elements) > 1:
        html = "".join(elements)
    else:
        html = elements[0]
    if wrap:
        html = f"<div>{html}</div>"
    ret: PageElement = tuple(BeautifulSoup(html, "html.parser").children)[0]
    if not isinstance(ret, Tag):
        raise ValueError(f"Given {html=} cannot be converted into bs4.Tag")  # pragma: no cover
    return ret


class ResRootProvider(Protocol):
    def __call__(self, caller_name: str) -> Path: ...


NONEXISTENT: str = "nonexistent"
