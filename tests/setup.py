from copy import deepcopy
from dataclasses import fields
from logging import disable as log_disable, NOTSET as LEVEL_NOTSET
from asyncio import iscoroutinefunction, run as async_run
from functools import partial
from pathlib import Path
from pickle import dump as pickle_dump
from json import dump as json_dump, dumps as json_dumps
from typing import Optional, Any, Type, Callable, TextIO
from os import remove as remove_file, environ

from bs4 import Tag, BeautifulSoup
from dotenv import load_dotenv

# Article is imported explicitly, whereas others are left behind a namespace as this file is not specifically testing
# them.
import wprss.webscraping as ws
from wprss.webscraping import Article
from wprss.telegram_bot import MessageHandler
from tests.test_utils import get_resource_root, ResRootProvider, NONEXISTENT
from tests.telegram_bot.utils import get_mh as get_mh_unsuppressed


# Use pickle only here, ensuring no data that comes from off-site gets unpickled, for security purposes.
def create_dumps(res_root: Path, *, test_input_path: Optional[str] = None, input_contents: Optional[str] = None,
                 object_to_pickle: Any,
                 pickle_path: str = "expected.pickle") -> None:
    if test_input_path:
        assert input_contents, "String conversion test specified via test_input_path without contents"
        with open(res_root / test_input_path, "a") as f:
            f.write(input_contents)
    with open(res_root / pickle_path, "wb") as p:
        pickle_dump(object_to_pickle, p)


# Trust the implementations of functions here, as this is only meant to be run once.
# If the rest of the tests pass after this having run, that confirms that the setup also behaves as expected.


def get_res_root_global(tested_file: str, caller_name: str) -> Path:
    res_root: Path = get_resource_root(tested_file=tested_file, caller_name=caller_name)
    res_root.mkdir(parents=True, exist_ok=True)
    return res_root


# There must exist a(n) (combination of) ABC(s) to represent JSONable types, but as this is for testing purposes and
# not subject to the stricter type annotating standards of the main code, dict[Any, Any] will do.
def serialize_list_to_json[T](objects: list[T], fp: TextIO,
                              serializer: Callable[[T], dict[Any, Any]]) -> None:
    json_dump(list(map(serializer, objects)), fp)


serialize_articles_to_json: partial[None] = partial(serialize_list_to_json, serializer=Article.serialize)


class Webscraping:
    get_res_root: ResRootProvider = partial(get_res_root_global, tested_file="webscraping")

    def create_get_article_from_tag(self) -> None:
        res_root: Path = self.get_res_root(caller_name="get_article_from_tag")
        for page_element in ws.get_article_root(ws.load_soup()):
            if not isinstance(page_element, Tag):
                continue
            article: Optional[Article] = ws.get_article_from_tag(page_element)
            if not isinstance(article, Article):
                continue
            create_dumps(res_root, test_input_path="tag.html", input_contents=str(page_element),
                         object_to_pickle=article)
            return

    @staticmethod
    def from_soup(caller_name: str, transformer: Callable[[BeautifulSoup], Any]) -> None:
        res_root = Webscraping.get_res_root(caller_name=caller_name)
        soup: BeautifulSoup = ws.load_soup()
        article_root = transformer(soup)
        create_dumps(res_root, test_input_path="complete_page.html", input_contents=str(soup),
                     object_to_pickle=article_root)

    def create_test_get_article_root(self) -> None:
        self.from_soup(caller_name="get_article_root", transformer=ws.get_article_root)

    def create_test_extract_articles(self) -> None:
        self.from_soup(
            caller_name="extract_articles",
            transformer=lambda s: ws.extract_articles(ws.get_article_root(s))
        )


class TelegramBot:
    get_res_root: ResRootProvider = partial(get_res_root_global, tested_file="telegram_bot")

    @staticmethod
    def get_mh(*args: Any, **kwargs: Any) -> MessageHandler:
        log_disable()
        mh: MessageHandler = get_mh_unsuppressed(*args, **kwargs)
        log_disable(LEVEL_NOTSET)
        return mh

    async def request_articles(self, res_root: Path, *, pickle_path: str,
                               json_path: Optional[str] = "known_articles.json") -> list[Article]:
        if json_path is None:
            json_path = NONEXISTENT
        mh: MessageHandler = self.get_mh(known_articles_file=res_root / json_path)
        articles: list[Article] = await mh.wait_for_and_get_updates()
        create_dumps(res_root, object_to_pickle=articles, pickle_path=pickle_path)
        if json_path == NONEXISTENT:
            try:
                remove_file(res_root / NONEXISTENT)
            except FileNotFoundError:
                pass
        else:
            with open(mh.known_articles_file, "w") as f:
                serialize_articles_to_json(articles, f)
        return articles

    @staticmethod
    def request_chats(res_root: Path, *, pickle_path: str = "known_chats.pickle",
                      known_chats_file: str = "known_chats.json") -> None:
        mock_known_chats: list[int] = [1, 2, 3, -1, 42]
        create_dumps(res_root, object_to_pickle=mock_known_chats, test_input_path=known_chats_file,
                     input_contents=json_dumps(mock_known_chats), pickle_path=pickle_path)

    async def create_test_populate_known_articles(self) -> None:
        res_root: Path = self.get_res_root(caller_name="populate_known_articles")
        await self.request_articles(res_root, pickle_path="expected.pickle")

    def create_test_load_known_chats(self) -> None:
        res_root: Path = self.get_res_root(caller_name="release_get_groups")
        self.request_chats(res_root, pickle_path="expected.pickle")

    async def create_test_wait_for_and_get_updates(self) -> None:
        res_root: Path = self.get_res_root(caller_name="wait_for_and_get_updates")
        articles: list[Article] = await self.request_articles(
            res_root,
            pickle_path="complete_articles.pickle",
            json_path="complete_articles.json"
        )
        with open(res_root / "known_articles_w_omission.json", "w") as f:
            serialize_articles_to_json(articles[1:], f)

    async def create_test_send_message(self) -> None:
        res_root: Path = self.get_res_root(caller_name="send_message")
        self.request_chats(res_root)
        articles: list[Article] = await self.request_articles(res_root, pickle_path="imaged_articles.pickle",
                                                              json_path=None)
        imageless_articles: list[Article] = deepcopy(articles)
        assert "image_link" in {field.name for field in fields(Article)}
        for article in imageless_articles:
            object.__setattr__(article, "image_link", None)
        create_dumps(res_root, object_to_pickle=imageless_articles, pickle_path="imageless_articles.pickle")

    async def create_test_run(self) -> None:
        res_root: Path = self.get_res_root(caller_name="run")
        await self.request_articles(res_root, pickle_path="articles.pickle", json_path=None)
        self.request_chats(res_root)


async def main() -> None:
    if not Path("./.env").exists():
        raise ValueError(".env file not present, fill .env_template and rename it to .env to proceed.")
    if environ.get("WEBSITE", None) is None:
        raise ValueError("WEBSITE environment variable is not configured correctly")
    # IGNORE: Annotating each class as a type argument to typing.Type as a Union would make little sense with the exact
    #         same listing being done a few characters to the left.
    classes: set[Type] = {Webscraping, TelegramBot}  # type: ignore[type-arg]
    for cls in classes:
        # See above for IGNORE.
        instance: Webscraping | TelegramBot = cls()  # type: ignore[type-arg]
        for method_name in dir(instance):
            if method_name.startswith("create") and callable(method := getattr(instance, method_name)):
                if not iscoroutinefunction(method):
                    method()
                else:
                    await method()


if __name__ == "__main__":
    load_dotenv()
    async_run(main())
