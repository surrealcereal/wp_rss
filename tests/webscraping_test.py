from functools import partial
from inspect import cleandoc
from pathlib import Path
from random import random
from typing import Callable, Optional, Self
from os import environ
from json import dumps as json_dumps, loads as json_loads

from bs4 import Tag, BeautifulSoup
from pytest import raises

from wprss.webscraping import Article, extract_articles, get_article_root, get_article_from_tag, load_soup
from tests.test_utils import compare_with_pickled, get_resource_root, get_first_tag, ResRootProvider

bs = partial(BeautifulSoup, features="html.parser")
get_res_root: ResRootProvider = partial(get_resource_root, tested_file="webscraping")


def test_if_article_equality_passes() -> None:
    # Also check for Optional[str] on Article.image_link
    different: Callable[[], str] = lambda: str(random())
    same_title: str = "same_title"
    same_desc: str = "same_desc"
    same_link: str = "same_link"
    same_image_link: str = "same_img_link"
    # Same title and desc -> same article
    assert Article(
        title=same_title, description=same_desc, link=different(), image_link=None
    ) == Article(
        title=same_title, description=same_desc, link=different(), image_link=None
    )
    # Different titles
    assert Article(
        title=different(), description=same_desc, link=same_link, image_link=same_image_link
    ) != Article(
        title=different(), description=same_desc, link=same_link, image_link=same_image_link
    )
    # Different descriptions
    assert Article(
        title=same_title, description=different(), link=same_link, image_link=same_image_link
    ) != Article(
        title=same_title, description=different(), link=same_link, image_link=same_image_link
    )
    # __hash__
    article1: Article = Article(
        title=same_title, description=same_desc, link=same_link, image_link=same_image_link
    )
    article2: Article = Article(
        title=same_title, description=same_desc, link=different(), image_link=None
    )
    test_dict: dict[Article, int] = dict()
    test_dict[article1] = 42
    assert test_dict[article2] == 42


def test_article_json_serialization() -> None:
    article: Article = Article(title="Title", description="Desc", link="example.org", image_link=None)
    assert article == Article.from_dict(json_loads(json_dumps(article.serialize())))


class Test_get_article_from_tag:
    res_root: Path = Path("./tests/resources/webscraping/get_article_from_tag/")
    title: str = "<h2>Title</h2>"
    desc1: str = "<p>This is a description!</p>"
    desc2: str = "<div class='default-style'>This is also a description!</div>"
    link: str = "<a href='example.org'>"
    image_link: str = "<img src='example.org/img'>"

    def test_invalid_html(self) -> None:
        title: str = self.title
        desc1: str = self.desc1

        empty: Tag = get_first_tag("<h>\n<h>")
        assert get_article_from_tag(empty) is None
        no_title: Tag = get_first_tag(desc1)
        assert get_article_from_tag(no_title) is None
        no_desc_p: Tag = get_first_tag(title)
        assert get_article_from_tag(no_desc_p) is None
        no_link: Tag = get_first_tag(title, desc1)
        assert get_article_from_tag(no_link) is None

    def test_well_formed_html(self) -> None:
        title: str = self.title
        desc1: str = self.desc1
        desc2: str = self.desc2
        link: str = self.link
        image_link: str = self.image_link

        well_formed: Optional[Article] = get_article_from_tag(
            get_first_tag(title, desc1, link)
        )
        assert isinstance(well_formed, Article)
        assert well_formed.title == "Title"
        assert well_formed.description == "This is a description!"
        assert well_formed.link == "example.org"
        assert well_formed.image_link is None
        well_formed_alternative: Optional[Article] = get_article_from_tag(
            get_first_tag(title, desc2, link, image_link)
        )
        assert isinstance(well_formed_alternative, Article)
        assert well_formed_alternative.description == "This is also a description!"
        assert well_formed_alternative.image_link == "example.org/img"

    def test_formatted_title_detection(self) -> None:
        desc2: str = self.desc2
        link: str = self.link
        image_link: str = self.image_link

        split_title_test: Optional[Article] = get_article_from_tag(
            get_first_tag("<h2>Title <b>bold</b></h2>", desc2, link, image_link)
        )
        assert isinstance(split_title_test, Article)
        assert split_title_test.title == "Title bold"

    def test_real_world(self) -> None:
        # get_article_from_tag(self.get_first_tag(html))
        compare_with_pickled(self.res_root, consume_path="tag.html",
                             tested_func=get_article_from_tag, input_transformer=get_first_tag)


def test_get_article_root() -> None:
    # get_article_root(BeautifulSoup(html))
    res_root: Path = get_res_root(caller_name="get_article_root")
    compare_with_pickled(res_root, consume_path="complete_page.html",
                         tested_func=get_article_root, input_transformer=bs)


def test_extract_articles() -> None:
    # extract_articles()
    res_root: Path = get_res_root(caller_name="extract_articles")
    compare_with_pickled(res_root, consume_path="complete_page.html",
                         tested_func=extract_articles,
                         input_transformer=lambda html: get_article_root(bs(html)))


def test_load_soup() -> None:
    # res_root: Path = get_res_root(caller_name="extract_articles")
    environ.pop("WEBSITE", None)
    with raises(ValueError):
        load_soup()
    environ["WEBSITE"] = "https://example.org"
    soup = load_soup()
    example_org_contents: str = """\
    Example Domain
    Example Domain
    This domain is for use in illustrative examples in documents. You may use this
        domain in literature without prior coordination or asking for permission.
    More information..."""
    text: str = "\n".join(x for x in soup.text.splitlines() if x.strip())
    example_org_contents = cleandoc(example_org_contents)  # remove indentation
    assert text == example_org_contents
