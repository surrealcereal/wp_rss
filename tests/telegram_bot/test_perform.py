import logging as log
from asyncio import run as async_run
from functools import partial
from json import load as json_load
from os import environ, remove as remove_file
from pathlib import Path
from pickle import load as pickle_load
from typing import Callable

from _pytest.logging import LogCaptureFixture
from pytest import raises, mark
from telegram import Message
from telegram.constants import ParseMode

from wprss.telegram_bot import getenv_or_raise, MessageHandler, MessageKwargs
from wprss.webscraping import Article
from tests.telegram_bot.utils import MockBot, BriefLogRecord, assert_log_from_caplog, get_mh, \
    assert_article_messages_sent, run_loop_once
from tests.test_utils import get_resource_root, ResRootProvider, NONEXISTENT


def test_getenv_or_raise() -> None:
    environ.pop("TESTING", None)
    with raises(EnvironmentError):
        getenv_or_raise("TESTING")
    environ["TESTING"] = "Testing"
    assert getenv_or_raise("TESTING") == "Testing"


get_res_root: ResRootProvider = partial(get_resource_root, tested_file="telegram_bot")
type PackedResult = tuple[MessageKwargs, Article, str]


class TestMessageHandler:
    def test___init__(self, caplog: LogCaptureFixture) -> None:
        # assert_log: partial[None] = partial(assert_log_from_caplog, captured_log=caplog)
        assert_log: Callable[[int, str], None] = lambda level, message: assert_log_from_caplog(
            BriefLogRecord(level, message), captured_log=caplog)
        caplog.set_level(log.DEBUG)
        environ["DEBUG"] = "0"
        assert not get_mh().debug
        assert_log(log.INFO, "Running in release mode")
        environ["DEBUG"] = "1"
        assert get_mh().debug
        assert_log(log.DEBUG, "Running in debug mode")
        environ.pop("DEBUG", None)
        assert not get_mh().debug
        assert_log(log.WARNING, "DEBUG is not set")

    def test_populate_known_articles(self) -> None:
        res_root = get_res_root(caller_name="populate_known_articles")
        mh: MessageHandler = get_mh()
        mh.populate_known_articles()
        assert not mh.known_articles
        mh = get_mh(known_articles_file=res_root / "known_articles.json")
        with open(res_root / "expected.pickle", "rb") as p:
            expected: list[Article] = pickle_load(p)
        assert mh.known_articles == expected

    class Test_release_get_groups:
        res_root = get_res_root(caller_name="release_get_groups")

        @mark.asyncio
        async def test_load_known_chats(self) -> None:
            mh: MessageHandler = get_mh(known_chats_file=self.res_root / "known_chats.json")
            groups: list[int] = await mh.release_get_chats()
            with open(self.res_root / "expected.pickle", "rb") as p:
                expected: list[int] = pickle_load(p)
            assert groups == expected

        @mark.asyncio
        async def test_update_processing(self, caplog: LogCaptureFixture) -> None:
            assert_log: partial[None] = partial(assert_log_from_caplog, captured_log=caplog)
            mh: MessageHandler = get_mh()
            assert not await mh.release_get_chats()
            caplog.clear()
            mh = get_mh(bot=MockBot(create_updates=True))
            groups: list[int] = await mh.release_get_chats()
            try:
                assert groups == [1, 2]
                assert_log(
                    BriefLogRecord(log.WARNING, "is not set"),
                    BriefLogRecord(log.INFO, "New chat discovered"),
                    BriefLogRecord(log.INFO, "name=<not found>"),
                    BriefLogRecord(log.INFO, "New chats saved")
                )
            finally:
                remove_file(NONEXISTENT)

    def test_debug_get_groups(self) -> None:
        environ.pop("DEBUG_CHATS", None)
        environ["DEBUG_CHATS"] = "1 2 3 4"
        mh: MessageHandler = get_mh()
        assert mh.debug_get_groups() == [1, 2, 3, 4]
        environ["DEBUG_CHATS"] = "1 2 3 A"
        with raises(ValueError) as invalid_chats:
            mh.debug_get_groups()
        assert "DEBUG_CHATS environment variable" in str(invalid_chats)

    @mark.asyncio
    async def test_get_groups(self) -> None:
        environ["DEBUG"] = "1"
        environ["DEBUG_CHATS"] = "1 2 3"
        mh: MessageHandler = get_mh()
        assert await mh.get_groups() == mh.debug_get_groups()
        environ["DEBUG"] = "0"
        environ.pop("DEBUG_CHATS", None)
        mh = get_mh()
        assert await mh.get_groups() == await mh.release_get_chats()

    class Test_wait_for_and_get_updates:

        res_root = get_res_root(caller_name="wait_for_and_get_updates")
        with open(res_root / "complete_articles.pickle", "rb") as p:
            complete_articles: list[Article] = pickle_load(p)

        def article_source(self) -> list[Article]:
            return self.complete_articles

        @mark.asyncio
        async def test_new_articles_present(self) -> None:
            mh: MessageHandler = get_mh(known_articles_file=self.res_root / "known_articles_w_omission.json",
                                        article_source=self.article_source)
            new_articles: list[Article] = await mh.wait_for_and_get_updates()
            assert new_articles == [self.complete_articles[0]]

        @mark.asyncio
        async def test_no_new_articles_found(self, caplog: LogCaptureFixture) -> None:
            caplog.set_level(log.DEBUG)
            assert_log: Callable[[int, str], None] = lambda level, message: assert_log_from_caplog(
                BriefLogRecord(level, message), captured_log=caplog)
            mh: MessageHandler = get_mh(known_articles_file=self.res_root / "complete_articles.json",
                                        article_source=self.article_source)
            await run_loop_once(mh.wait_for_and_get_updates())
            assert_log(log.INFO, "No new articles")

    class Test_prepare_message:
        DESC_LIMIT: int = 20

        @staticmethod
        def get_desc_of_len(length: int) -> str:
            return " ".join(map(str, range(length)))

        @staticmethod
        def article_w_desc_of_len(desc: str) -> Article:
            return Article(
                description=desc,
                title="Title",
                link="example.org",
                image_link="example.org/img"
            )

        def get_message(self, desc_len: int, desc_suffix: str = "") -> PackedResult:
            result: MessageKwargs = get_mh(desc_limit=self.DESC_LIMIT).prepare_message(
                article := self.article_w_desc_of_len(
                    desc := self.get_desc_of_len(desc_len) + desc_suffix
                )
            )
            return result, article, desc

        @staticmethod
        def extract_trimmed_desc(caption: str) -> str:
            return caption.split("</b>")[1].lstrip().split("\n")[0]

        def test_desc_not_trimmed(self) -> None:
            result, article, desc = self.get_message(self.DESC_LIMIT - 1)
            assert result["caption"] == f"<b>{article.title}</b>\n\n{desc}\n\n{article.link}"
            assert result["photo"] == f"example.org/img"
            assert result["parse_mode"] == ParseMode.HTML

        def test_img_link_absent(self) -> None:
            result: MessageKwargs = get_mh(desc_limit=self.DESC_LIMIT).prepare_message(
                Article(
                    title="Title",
                    description="desc",
                    link="example.org",
                    image_link=None
                )
            )
            assert result.pop("photo", None) is None

        def test_desc_trimmed(self) -> None:
            result, _, _ = self.get_message(self.DESC_LIMIT + 2)
            trimmed_desc: str = self.get_desc_of_len(self.DESC_LIMIT) + " (...)"
            caption: str | int = result["caption"]
            assert isinstance(caption, str)
            assert self.extract_trimmed_desc(caption) == trimmed_desc

        def test_desc_three_dots_in_raw_desc(self) -> None:
            result, article, desc = self.get_message(self.DESC_LIMIT - 1, desc_suffix="…")
            trimmed_desc = self.get_desc_of_len(self.DESC_LIMIT - 1) + " (...)"
            caption: str | int = result["caption"]
            assert isinstance(caption, str)
            assert self.extract_trimmed_desc(caption) == trimmed_desc

    class Test_send_message:
        res_root: Path = get_res_root(caller_name="send_message")
        messages_sent: list[Message] = list()
        mb: MockBot = MockBot(messages_sent=messages_sent)
        mh: MessageHandler = get_mh(
            bot=mb,
            known_chats_file=res_root / "known_chats.json"
        )
        mh.known_chats = async_run(mh.release_get_chats())
        with open(res_root / "known_chats.pickle", "rb") as p:
            known_chats: list[int] = pickle_load(p)

        async def send_and_check(self, articles_pickle_name: str) -> None:
            with open(self.res_root / articles_pickle_name, "rb") as p:
                articles: list[Article] = pickle_load(p)
            await self.mh.send_messages(articles)
            assert_article_messages_sent(chats=self.known_chats, articles=articles, messages=self.messages_sent,
                                         message_preparer=self.mh.prepare_message)

        @mark.asyncio
        async def test_send_with_photo(self) -> None:
            await self.send_and_check("imaged_articles.pickle")

        @mark.asyncio
        async def test_send_without_photo(self) -> None:
            await self.send_and_check("imageless_articles.pickle")

    class Test_run:

        res_root: Path = get_res_root(caller_name="run")
        with open(res_root / "articles.pickle", "rb") as p:
            articles: list[Article] = pickle_load(p)
        with open(res_root / "known_chats.pickle", "rb") as p:
            known_chats: list[int] = pickle_load(p)

        def article_source(self) -> list[Article]:
            return self.articles

        @mark.asyncio
        async def test_without_sending(self, caplog: LogCaptureFixture) -> None:
            assert_log: partial[None] = partial(assert_log_from_caplog, captured_log=caplog)
            caplog.set_level(log.DEBUG)
            mh: MessageHandler = get_mh(article_source=self.article_source, known_articles_file="tmp.json")

            await run_loop_once(mh.run())
            with open(mh.known_articles_file, "r") as f:
                saved_articles = list(map(Article.from_dict, json_load(f)))
            remove_file("tmp.json")
            assert_log(
                BriefLogRecord(log.INFO, "Discovered new"),
                BriefLogRecord(log.WARNING, "no chats are known"),
                BriefLogRecord(log.INFO, "Saved new")
            )
            assert self.articles == mh.known_articles[::-1]
            assert self.articles == saved_articles[::-1]

        @mark.asyncio
        async def test_with_send(self, caplog: LogCaptureFixture) -> None:
            messages_sent: list[Message] = list()
            mh: MessageHandler = get_mh(
                article_source=self.article_source,
                bot=MockBot(messages_sent=messages_sent),
                known_chats_file=self.res_root / "known_chats.json"
            )
            await run_loop_once(mh.run())
            remove_file(NONEXISTENT)
            assert_article_messages_sent(chats=self.known_chats, articles=self.articles, messages=messages_sent,
                                         message_preparer=mh.prepare_message)
