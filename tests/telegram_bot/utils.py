import builtins
import logging as log
from asyncio import wait_for
from collections import namedtuple
from datetime import datetime
from pathlib import Path
from typing import Coroutine, Any, Never, Protocol, Callable, Optional, Union, Iterable
from unittest.mock import patch

from _pytest.logging import LogCaptureFixture
from telegram import Bot, PhotoSize, Message, Update, Chat, User, ChatMember, ChatMemberUpdated
from telegram._utils.defaultvalue import DEFAULT_NONE
from telegram._utils.types import FileInput, ODVInput

from wprss.telegram_bot import MessageHandler, MessageKwargs
from wprss.webscraping import Article, get_articles
from tests.test_utils import NONEXISTENT


class MockBot(Bot):
    now: datetime = datetime.now()

    def __init__(self, create_updates: bool = False, messages_sent: Optional[list[Message]] = None) -> None:
        self._create_updates: bool = create_updates
        if messages_sent is None:
            self._messages_sent = list()
        else:
            self._messages_sent = messages_sent
        super().__init__("MOCKED_TOKEN")

    async def initialize(self) -> None:
        return None

    # IGNORE: It is possible to annotate these methods completely properly, however that depends on the internal
    #         implementation of the library, and depending on that not to change would be unwise, not to mention
    #         the visual clutter the plethora of defaulted - and for our purposes, irrelevant - arguments cause.
    async def send_photo(  # type: ignore[override]
            self, chat_id: int | str, photo: FileInput | PhotoSize | None, caption: Optional[str] = None,
            **kwargs: Any) -> Message:
        assert isinstance(chat_id, int)  # assert here rather than change method signature to keep parent signature
        msg: Message = Message(
            chat=Chat(id=chat_id, type="group"),
            message_id=0,
            caption=caption,
            date=self.now,
            photo=None  # left None explicitly, as this specific mock output is not meant to be consumed, and is just
            # here to conform to parent signature of returning coroutine to Message.
        )
        self._messages_sent.append(msg)
        return msg

    # See above for IGNORE.
    async def send_message(  # type: ignore[override]
            self,
            chat_id: Union[int, str],
            text: str,
            parse_mode: ODVInput[str] = DEFAULT_NONE,
            **kwargs: Any
    ) -> Message:
        message: Message = await self.send_photo(chat_id, photo=None, caption=text)
        return message

    async def get_updates(self, *args: Any, **kwargs: Any) -> tuple[Never | Update, ...]:
        if not self._create_updates:
            return tuple()
        mocked_chat_1: Chat = Chat(id=1, title="<mocked chat 1>", type="group")
        mocked_chat_2: Chat = Chat(id=2, title=None, type="group")
        mocked_user: User = User(id=42, first_name="<mocked user>", is_bot=False)
        mocked_member: ChatMember = ChatMember(user=mocked_user, status="<mocked>")
        return (
            Update(
                update_id=0,
                my_chat_member=ChatMemberUpdated(
                    chat=mocked_chat_1,
                    from_user=mocked_user,
                    date=self.now,
                    old_chat_member=mocked_member,
                    new_chat_member=mocked_member
                )
            ),
            Update(
                update_id=1,
                message=Message(
                    message_id=3,
                    date=self.now,
                    chat=mocked_chat_2
                )
            )
        )


level_const_to_str: dict[int, str] = {
    log.DEBUG: "DEBUG",
    log.INFO: "INFO",
    log.WARNING: "WARNING",
    log.ERROR: "ERROR",
    log.CRITICAL: "CRITICAL"
}

BriefLogRecord = namedtuple("BriefLogRecord", ("levelname", "message"))


def are_logs_equal(record: log.LogRecord, brief_record: BriefLogRecord) -> bool:
    return record.levelname == level_const_to_str[brief_record.levelname] and brief_record.message in record.message


def assert_log_from_caplog(*records: BriefLogRecord, captured_log: LogCaptureFixture) -> None:
    try:
        i: int = -1
        for i, record in enumerate(captured_log.records):
            if are_logs_equal(record, brief_record=records[0]):
                break
        for record, brief_record in zip(captured_log.records[i:], records):
            assert are_logs_equal(record, brief_record)
        assert i >= 0, f"{'Chain' if len(records) > 1 else 'Message'} appears nowhere in captured log records"
    finally:
        captured_log.clear()


mock_bot: MockBot = MockBot()


class MessageHandlerCreator(Protocol):
    def __call__(self, bot: Bot = mock_bot, check_interval: int = 10, desc_limit: int = 25,
                 known_chats_file: str | Path = NONEXISTENT,
                 known_articles_file: str | Path = NONEXISTENT,
                 article_source: Callable[[], list[Article]] = get_articles) -> MessageHandler: ...


def get_mh(bot: Bot = mock_bot, check_interval: int = 10, desc_limit: int = 25,
           known_chats_file: str | Path = NONEXISTENT, known_articles_file: str | Path = NONEXISTENT,
           article_source: Callable[[], list[Article]] = get_articles) -> MessageHandler:
    with patch.object(builtins, "input", lambda _: "\n"):
        return MessageHandler(
            bot=bot,
            check_interval=check_interval,
            desc_limit=desc_limit,
            known_chats_file=known_chats_file,
            known_articles_file=known_articles_file,
            article_source=article_source
        )


def assert_article_messages_sent(*, chats: Iterable[int],
                                 articles: Iterable[Article], messages: list[Message],
                                 message_preparer: Callable[[Article], MessageKwargs]) -> None:
    def get_caption(article: Article) -> str:
        msg: MessageKwargs = message_preparer(article)
        text_attribute: str = "caption" if "caption" in msg else "text"
        ret: str | int = msg[text_attribute]
        assert isinstance(ret, str)
        return ret

    captions: set[str] = set(map(get_caption, articles))
    assert messages
    for chat in chats:
        for caption in captions:
            assert sum(1 for msg in messages if msg.caption == caption and msg.chat_id == chat) == 1
    messages.clear()


async def run_loop_once(fut: Coroutine[Any, Any, Any], timeout: float = 0.1) -> None:
    try:
        await wait_for(fut, timeout)
    except TimeoutError:
        pass
