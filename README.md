## wprss
[![python](https://img.shields.io/badge/python-3.12-blue?logo=python)](https://www.python.org/downloads/release/python-3120/)
[![Checked with mypy](https://www.mypy-lang.org/static/mypy_badge.svg)](https://mypy-lang.org/)
[![mypy](https://img.shields.io/badge/mypy_version-1.11.0%2Bdev.415d49f25b6315cf1b7a04046a942246a033498d-blue)](https://github.com/mypyc/mypy_mypyc-wheels/releases/tag/v1.11.0%2Bdev.415d49f25b6315cf1b7a04046a942246a033498d)
[![coverage](https://img.shields.io/badge/coverage-100%25-49c31c)](https://codeberg.org/surrealcereal/wprss)

Fully mypy compliant Python script to scrape WordPress sites with article feeds that
do not support RSS into a machine-parsable format. Also included is a 
Telegram bot to demonstrate how to consume the output stream.

### Disclaimer!
May not work with all WordPress sites, has been developed for specific personal use.

### How to use
After setting up the required dependencies via `pip install -r requirements.txt`,
set the empty fields in the `.env_template` file, rename it to `.env` and run either `python -m wprss.webscraping` or 
`python -m wprss.telegram_bot`, depending on your needs.

### How to run tests
Run the provided `./run_tests`, and it will check static typing, run tests and check coverage and will install
dependencies whenever required.

Note that `./run_tests` enforces a check to see if it's running from a virtualenv, and will refuse to execute if not.
To bypass this check, run the script via `NOVENV=1 ./run_tests`.