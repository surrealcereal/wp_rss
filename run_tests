#!/bin/sh

if ! [ "$NOVENV" = "1" ] && [ -z "$VIRTUAL_ENV" ]; then
  >&2 echo "Error: not in virtualenv."
  exit 1
fi

if pip freeze -r requirements.txt 2>&1 | grep "not installed" -q; then
  echo "Required runtime dependencies not found, installing"
  pip install -r requirements.txt
fi

if ! hash mypy coverage pytest 2>/dev/null; then
  echo "Required dependencies for testing not found, installing"
  pip install -r requirements_testing.txt
fi

# Refresh to override any potential system installations of the above executables.
. "$VIRTUAL_ENV"/bin/activate

echo "Running mypy"
venv="$(basename "$VIRTUAL_ENV")"
mypy . --exclude "$venv"
echo
if ! [ -d ./tests/resources ]; then
  printf "Running setup for tests..."
  python -m tests.setup || exit 1
  printf "  DONE\n"
fi
if [ -e .coverage ]; then
  coverage erase
fi
echo "Running tests"
coverage run -m pytest -qq .
echo
echo "Testing coverage"
cov_perc="$(coverage report --format=total)"
echo ""$cov_perc"%"