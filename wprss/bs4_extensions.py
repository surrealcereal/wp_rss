from functools import partial
from typing import Type, Optional

from bs4 import Tag, NavigableString


def optional_capable_find[T](element: Tag, name: str, resulting_type: Type[T], optional: bool, **attrs: str) \
        -> T | Optional[T]:  # explicitly denote either T or Optional of T.
    # Undo the workaround used by bs4
    if "class_" in attrs.keys():
        attrs["class"] = attrs.pop("class_")
    # The types for Tag.find are broken, mypy just doesn't do any checking and thus does not complain even when
    # impossible types are annotated for attrs in the function signature, such as **attrs: Never.
    # So the current approach is to just use str as the annotation, and update it as other types appear as filter
    # arguments throughout the codebase.
    # A ParamSpec approach where Tag.find's exact kwargs type is captured through an unused _: Callable[P, R] and
    # attrs is annotated as ... /, *unused_args_for_paramspec: P.args, **attrs: P.kwargs) -> ...
    # could have been useful if Tag.find was also not broken in allocating types to passed .find(*args, **kwargs),
    # and insisted on:
    # Argument 2 to "find" of "Tag" has incompatible type "**P.kwargs"; expected "bool" [arg-type] (???)
    #         (written on 2024-06-01, using mypy version 1.11.0+dev.f60f458bf0e75e93a7b23a6ae31afd18f3d201e3)
    result = element.find(name, attrs)
    if result is None:
        if optional:
            return None
        raise ValueError(f"Search with parameters {name=}, {attrs=} returned None")
    if not isinstance(result, resulting_type):
        if optional:
            return None
        raise ValueError(f"Found {result} but not of type {resulting_type}, instead of {type(result)}")
    return result


# IGNORE: functools.partial is currently bugged, see: https://github.com/python/mypy/issues/17301
#         (written on 2024-05-31, using mypy version 1.11.0+dev.f60f458bf0e75e93a7b23a6ae31afd18f3d201e3)
type FindResult = Tag | NavigableString
find_or_raise: partial[FindResult] = partial(  # type: ignore[assignment]
    optional_capable_find, optional=False  # type: ignore[arg-type]
)
find_tag_or_raise: partial[Tag] = partial(find_or_raise, resulting_type=Tag)  # type: ignore[assignment, arg-type]
find_tag: partial[Optional[Tag]] = partial(
    optional_capable_find, resulting_type=Tag,  # type: ignore[assignment, arg-type]
    optional=True
)


def get_str_or_raise(element: Tag, name: str) -> str:
    if not (ret := element.get(name)):
        raise ValueError(f"{name=} not found in supplied element")
    assert isinstance(ret, str)
    return ret
