from dataclasses import dataclass, field, asdict
from functools import partial
from os import getenv
from typing import Iterable, Optional, cast, Self

from bs4 import BeautifulSoup, Tag, PageElement
from dotenv import load_dotenv
from requests import get as requests_get

from wprss.bs4_extensions import find_tag_or_raise, find_tag, get_str_or_raise


@dataclass(frozen=True)
class Article:
    title: str
    description: str
    link: str = field(compare=False, hash=False)
    image_link: Optional[str] = field(compare=False, hash=False)

    @classmethod
    def from_dict(cls, data: dict[str, str]) -> Self:
        return cls(
            title=data["title"],
            description=data["description"],
            link=data["link"],
            image_link=data["image_link"]
        )

    def serialize(self) -> dict[str, str | Optional[str]]:
        return asdict(self)


def get_article_from_tag(div: Tag) -> Optional[Article]:
    find: partial[Optional[Tag]] = partial(find_tag, div)
    if not len(div.text.replace('\n', '')):
        return None
    img_candidate: Optional[Tag]
    desc_candidate: Optional[Tag]
    link_candidate: Optional[Tag]
    title_candidate: Optional[Tag]
    image_link: Optional[str] = None
    if not (title_candidate := find("h2")):
        return None

    if not (desc_candidate := find("p")):
        desc_candidate = find("div", class_="default-style")
        if not desc_candidate:
            return None

    if not (link_candidate := find("a")):
        return None
    link_candidate = cast(Tag, link_candidate)

    if img_candidate := find("img"):
        image_link = get_str_or_raise(img_candidate, "src")

    title_components: list[str] = list()
    component: PageElement
    for component in title_candidate:
        title_components.append(component.text)
    title: str = "".join(title_components)
    description: str = desc_candidate.contents[0].text
    link: str = get_str_or_raise(link_candidate, "href")
    return Article(title, description, link, image_link)


def load_soup() -> BeautifulSoup:
    website_link: Optional[str] = getenv("WEBSITE")
    if not website_link:
        raise ValueError("Link to website to parse has not been supplied")
    return BeautifulSoup(requests_get(website_link).content, "html.parser")


def get_article_root(soup: BeautifulSoup) -> Iterable[PageElement]:
    articles_root_first: Tag = find_tag_or_raise(soup, "div", class_="elementor elementor-2197")
    articles_root_second: Tag = find_tag_or_raise(articles_root_first, "div", class_="elementor-section-wrap")
    return articles_root_second.children


def extract_articles(page_elements: Iterable[PageElement]) -> list[Article]:
    articles: list[Article] = list()
    page_element: PageElement
    for page_element in page_elements:
        if not isinstance(page_element, Tag):
            continue
        article = get_article_from_tag(page_element)
        if not isinstance(article, Article):
            continue
        articles.append(article)
    return articles


def get_articles() -> list[Article]:  # pragma: no cover
    return extract_articles(get_article_root(load_soup()))


def main() -> None:  # pragma: no cover
    load_dotenv()
    print(extract_articles(get_article_root(load_soup())))


if __name__ == "__main__":  # pragma: no cover
    main()
