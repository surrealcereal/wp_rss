import logging
from asyncio import run as async_run, sleep as async_sleep
from json import load as json_load, dump as json_dump
from logging import basicConfig, info, warning, INFO as LEVEL_INFO, DEBUG as LEVEL_DEBUG
from os import getenv
from pathlib import Path
from sys import stderr
from traceback import TracebackException
from typing import NoReturn, Optional, Callable
from time import sleep

from dotenv import load_dotenv
from telegram import Bot, ChatMemberUpdated, Message, Chat
from telegram.constants import ParseMode
from telegram.error import TelegramError

from wprss.webscraping import Article, get_articles

type MessageKwargs = dict[str, str | int]


def getenv_or_raise(environment_variable: str) -> str:
    env_var: Optional[str] = getenv(environment_variable)
    if not env_var:
        raise EnvironmentError(f"The {environment_variable} environment variable is not defined")
    return env_var


class MessageHandler:
    def __init__(self, bot: Bot, check_interval: int, desc_limit: int, known_chats_file: str | Path,
                 known_articles_file: str | Path, article_source: Callable[[], list[Article]] = get_articles):
        # list chosen whenever possible for json-serializability
        self.bot = bot
        self.check_interval = check_interval
        self.desc_limit = desc_limit
        self.article_source = article_source
        self.known_chats_file: Path = Path(known_chats_file) if isinstance(known_chats_file, str) else known_chats_file
        self.known_articles_file: Path = Path(known_articles_file) if isinstance(known_articles_file,
                                                                                 str) else known_articles_file
        self.known_articles: list[Article] = list()
        self.known_chats: list[int] = list()
        self.debug = (mode := getenv("DEBUG")) == "1"
        self.init_logging(improper_env=mode is None)
        self.populate_known_articles()

    def populate_known_articles(self) -> None:
        if self.known_articles_file.exists():
            with open(self.known_articles_file, "r") as f:
                self.known_articles = list(map(Article.from_dict, json_load(f)))

    def init_logging(self, improper_env: bool) -> None:
        basicConfig(
            format='%(asctime)s [%(levelname)s] %(message)s',
            level=LEVEL_INFO if not self.debug else LEVEL_DEBUG
        )
        logger = logging.getLogger(__name__)
        logger.propagate = True
        if self.debug:
            logger.debug("Running in debug mode.")
        else:
            logger.info("Running in release mode.")
            sleep(0.1)
            input("Allow proceeding in release mode by pressing any key... ")
        if improper_env:
            logger.warning("Running in release mode because DEBUG is not set, set it to 0 to disable this warning.")

    async def release_get_chats(self) -> list[int]:
        type ContainsChat = ChatMemberUpdated | Message

        known_chats: list[int] = list()
        chat_added: bool = False

        if self.known_chats_file.exists():
            with open(self.known_chats_file) as f:
                known_chats = json_load(f)

        def process(chat_container: ContainsChat) -> None:
            nonlocal chat_added
            chat: Chat = chat_container.chat
            chat_id: int = chat.id
            name: Optional[str] = chat.title
            if name is None:
                name = "<not found>"
            if chat.id not in known_chats:
                known_chats.append(chat_id)
                info(f"New chat discovered: {name=}, {chat_id=}")
                chat_added = True

        for update in (await self.bot.get_updates()):
            groups_joined: Optional[ChatMemberUpdated] = update.my_chat_member
            message_sent: Optional[Message] = update.message
            if isinstance(groups_joined, ChatMemberUpdated):
                process(groups_joined)
            elif isinstance(message_sent, Message):
                process(message_sent)

        if chat_added:
            self.known_chats_file.touch()
            with open(self.known_chats_file, "w") as f:
                json_dump(known_chats, f)
                info("New chats saved.")

        return known_chats

    @staticmethod
    def debug_get_groups() -> list[int]:
        debug_chats: str = ""
        try:
            return list(map(int, (debug_chats := getenv_or_raise("DEBUG_CHATS")).split(" ")))
        except ValueError:
            raise ValueError(f"Invalid DEBUG_CHATS environment variable specified: {debug_chats}")

    async def get_groups(self) -> list[int]:
        if self.debug:
            return self.debug_get_groups()
        return await self.release_get_chats()

    async def wait_for_and_get_updates(self) -> list[Article]:
        new_articles: list[Article] = [article for article in self.article_source() if
                                       article not in self.known_articles]
        while not new_articles:
            info("No new articles discovered.")
            await async_sleep(self.check_interval)
            # Tests return at this point by causing a ValueError, so continue cannot be covered with this method:
            # ignore it.
            continue  # pragma: no cover
        return list(reversed(new_articles))

    def prepare_message(self, article: Article) -> MessageKwargs:
        desc: str = article.description
        desc = desc.replace("…", "...")
        if len(desc.split(" ")) > self.desc_limit:
            desc = " ".join(desc.split(" ")[:self.desc_limit]) + " (...)"
        # Use a single the "there is more to read" symbol.
        if desc.endswith("..."):
            desc = desc.rstrip("...") + " (...)"
        text: str = f"<b>{article.title}</b>\n\n{desc}\n\n{article.link}"
        text_field_name: str = "text" if not article.image_link else "caption"
        kwargs: MessageKwargs = {
            text_field_name: text,
            "parse_mode": ParseMode.HTML
        }
        if article.image_link:
            kwargs["photo"] = article.image_link
        return kwargs

    async def send_messages(self, new_articles: list[Article]) -> None:
        article: Article
        for article in new_articles:
            kwargs: MessageKwargs = self.prepare_message(article)
            for chat in self.known_chats:
                kwargs["chat_id"] = chat
                async with self.bot:
                    try:
                        # IGNORE: I do not know of any method to get the kwargs of a function as a TypeAlias (or -like
                        #         thing) to use in annotations. If I were to accept an unused _: Callable[P, __]
                        #         and pass telegram.Bot.send_photo in there, that ParamSpec approach would only work
                        #         for other variables in the signature. However, P acts as a plain `object` inside the
                        #         function, so P.kwargs annotations basically mean Any.
                        if article.image_link:
                            await self.bot.send_photo(**kwargs)  # type: ignore[arg-type]
                        else:
                            await self.bot.send_message(**kwargs)  # type: ignore[arg-type]

                        self.known_articles.append(article)
                    # Not worth testing for as this output goes totally unconsumed elsewhere, and it does not affect
                    # program flow.
                    except TelegramError as e:  # pragma: no cover
                        tb: str = "".join(TracebackException.from_exception(e).format())
                        print(tb, file=stderr)

    async def run(self) -> NoReturn:
        while True:
            new_articles: list[Article] = await self.wait_for_and_get_updates()
            info("Discovered new articles!")
            self.known_chats = await self.get_groups()
            if self.known_chats:
                await self.send_messages(new_articles)
            else:
                warning("Cannot send discovered articles because no chats are known!")
            article: Article
            for article in new_articles:
                if article not in self.known_articles:
                    self.known_articles.append(article)
            with open(self.known_articles_file, "w") as f:
                json_dump(list(map(Article.serialize, self.known_articles)), f)
            info("Saved new articles into known articles buffer.")


async def main() -> None:  # pragma: no cover
    load_dotenv()
    await MessageHandler(
        bot=Bot(getenv_or_raise("TOKEN")),
        check_interval=5 * 60,
        desc_limit=25,
        known_chats_file="./known_chats.json",
        known_articles_file="./known_articles.json"
    ).run()


if __name__ == "__main__":  # pragma: no cover
    async_run(main())
